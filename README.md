# TOI-197

This report presents the PARAM results for TOI-197: a subgiant star with solar-like oscillations and a transiting planet identified by TESS (Transiting Exoplanet Survey Satellite). The results are published in [A Hot Saturn Orbiting an Oscillating Late Subgiant Discovered by TESS](https://ui.adsabs.harvard.edu/abs/2019AJ....157..245H/abstract) by [Huber et al. (2018)](https://ui.adsabs.harvard.edu/abs/2019AJ....157..245H/abstract).
